#define Name "DeSeRtod ModPack"
#define Patch "0.9.5"
#define Version "1.2"
#define Publisher "woole & fleshr"

#define Font "OpenSans.ttf"
#define FontName "Open Sans"

#include "CustomMessages.iss"
#include "Components.iss"
#include "Modules\SHFileOperation.iss"
#include "Modules\botva2.iss"

[Setup]
AppId={{F483B83F-4490-40C6-BD64-9801EB5129D5}
AppName={#Name} {#Patch}
AppVersion={#Version}
AppPublisher={#Publisher}
VersionInfoVersion={#Version}
VersionInfoCopyright={#Publisher}

SetupIconFile=Images\icon.ico
DefaultDirName={code:WotDir}
AppendDefaultDirName=no
DirExistsWarning=no
UninstallFilesDir={app}\DeSeRtod_ModPack

OutputDir=Output
OutputBaseFilename=DeSeRtod_ModPack

InternalCompressLevel=ultra64
Compression=lzma2/ultra64
SolidCompression=yes

DisableProgramGroupPage=yes
DisableReadyPage=yes
ShowComponentSizes=no
WizardImageFile=Images\what.bmp
WizardSmallImageFile=Images\what.bmp

ComponentsListTVStyle=yes
ComponentsListBtnImageFile=Images\checkbox.bmp

[Files]
Source: Files\*; DestDir: {tmp}; Flags: dontcopy
Source: dll\*; Flags: dontcopy
Source: {#Font}; Flags: dontcopy

[Languages]
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl"

[Code]
function WotDir(S:String): String;
var
  InsPath: String;
  er: boolean;
begin
  Result:=ExpandConstant('C:\Games\World_of_Tanks\');
  er:=RegQueryStringValue(HKLM, 'SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\{1EAC1D02-C6AC-4FA6-9A44-96258C37C812RU}_is1', 'InstallLocation', InsPath);
  if er and (InsPath<>'') then
    Result:=InsPath;
end;

var
ModsImg, AboutModsBgImg, ModsBgImg, CompListBgImg, DeleteImg, BackupImg, NoneImg, BackupBgImg, FolderImg, DirEditImg, BackgroundImg: Longint;
DeleteCB, BackupCB, NoneCb, BrowseBtn, FAQBtn, BigNextBtn, BackBtn, MinBtn, CloseBtn, CancelBtn, NextBtn:HWND;
Font: TFont;
AboutMods, AboutDeleteLabel, AboutBackupLabel, AboutNoneLabel, DeleteLabel, BackupLabel, NoneLabel, SelectDirLabel1, SelectDirLabel2, SelectDirLabel3 , SelectDirLabel4, VersionLabel, TitleLabel: TLabel;

#ifdef UNICODE
    #define A "W"
#else
    #define A "A"
#endif

const
  FR_PRIVATE = $10;

function AddFontResource(lpszFilename: String; fl, pdv: DWORD): Integer; external 'AddFontResourceEx{#A}@gdi32.dll stdcall';
function RemoveFontResource(lpFileName: String; fl, pdv: DWORD): BOOL; external 'RemoveFontResourceEx{#A}@gdi32.dll stdcall';

function KillTask(ExeFileName: AnsiString): Integer; external 'KillTask@files:ISTask.dll stdcall';
function RunTask(FileName: AnsiString; bFullpath: Boolean): Boolean; external 'RunTask@files:ISTask.dll stdcall';

function NextButtonClick(CurPageID: Integer): Boolean;
var
  ver:string;
begin
  Result := True;
  case CurPageID of wpWelcome:
    if RunTask('WorldOfTanks.exe', False) then
    if MsgBox('����� ������ ��������. ������� �� � ���������� ���������?', mbConfirmation, MB_YESNO) = IDYES then
      KillTask('WorldOfTanks.exe')
    else
      Result:=False;
  end;

  case CurPageID of wpSelectDir:
    if not FileExists(ExpandConstant('{app}\WorldOfTanks.exe')) then begin
      MsgBox('�� ����� ������� ����� World of Tanks!', mbError, MB_OK);
      Result:=False;
    end else begin
      GetVersionNumbersString(ExpandConstant('{app}\WorldOfTanks.exe'), ver);
      if not (ver = '{#Patch}.0') then begin
        MsgBox(ExpandConstant('������ �� �������� �� ������ ������ ����.'), mbError, MB_OK);
        Result:=False;
        exit;
      end;
    end;
  end;
end;

function DeleteCheck: Boolean;
begin
  If BtnGetChecked(DeleteCB) then begin
    DelTree(ExpandConstant('{app}\res_mods\{#Patch}'), True, True, True);
    DelTree(ExpandConstant('{app}\res_mods\xvm'), True, True, True);
    CreateDir(ExpandConstant('{app}\res_mods\{#Patch}'));
  end;
end;

function BackupCheck: Boolean;
begin
  If BtnGetChecked(BackupCB) then begin
    MoveDir(ExpandConstant('{app}\res_mods\{#Patch}\'),ExpandConstant('{app}\DeSeRtod_ModPack\backup\{#Patch}'));
    MoveDir(ExpandConstant('{app}\res_mods\xvm\'),ExpandConstant('{app}\DeSeRtod_ModPack\backup\xvm'));
    CreateDir(ExpandConstant('{app}\res_mods\{#Patch}'));
  end;
end;

function InitializeSetup:boolean;
begin
  if not FileExists(ExpandConstant('{tmp}\botva2.dll')) then ExtractTemporaryFile('botva2.dll');
  if not FileExists(ExpandConstant('{tmp}\CallbackCtrl.dll')) then ExtractTemporaryFile('CallbackCtrl.dll');
  Result:=True;
end;

procedure NextButtonOnClick(hBtn:HWND);
begin
  WizardForm.NextButton.OnClick(nil);
end;

procedure CancelButtonOnClick(hBtn:HWND);
begin
  WizardForm.CancelButton.OnClick(nil);
end;

procedure BackButtonOnClick(hBtn:HWND);
begin
  WizardForm.BackButton.OnClick(nil);
end;

procedure BrowseButtonOnClick(hBtn:HWND);
begin
  WizardForm.DirBrowseButton.OnClick(nil);
end;

procedure MinButtonOnClick(hBtn:HWND);
begin
  Application.Minimize;
end;

procedure FAQOnClick(hBtn:HWND);
var
  Form: TSetupForm;
  Edit: TRichEditViewer;
  CancelButton: TNewButton;
  RTF: AnsiString;
begin
  ExtractTemporaryFile('faq.rtf');
  LoadStringFromFile(ExpandConstant('{tmp}\faq.rtf'), RTF);

  Form := CreateCustomForm();
  with Form do begin
    ClientWidth:=ScaleX(540);
    ClientHeight:=ScaleY(360);
    Caption:='����� ���������� �������';
    CenterInsideControl(WizardForm, False);

  Edit := TRichEditViewer.Create(Form);
  with Edit do begin
    Parent:=Form;
    ScrollBars:=ssVertical;
    ReadOnly:=True;
    Left:=18;
    Top:=16;
    Width:=504;
    Height:=301;
    RTFText:=RTF;
  end;

  CancelButton := TNewButton.Create(Form);
  with CancelButton do begin
    Parent:=Form;
    Width:=ScaleX(75);
    Height:=ScaleY(23);
    Left:=Form.ClientWidth - ScaleX(75 + 19);
    Top:=Form.ClientHeight - ScaleY(23 + 10);
    Caption:='�������';
    ModalResult:=mrOk;
  end;
  ShowModal;
  Free;
  end;
end;

procedure DeleteOnClick(hBtn:HWND);
begin
  BtnSetChecked(DeleteCB, True);
  BtnSetChecked(BackupCB, False);
  BtnSetChecked(NoneCB, False);
end;
procedure BackupOnClick(hBtn:HWND);
begin
  BtnSetChecked(DeleteCB, False);
  BtnSetChecked(BackupCB, True);
  BtnSetChecked(NoneCB, False);
end;
procedure NoneOnClick(hBtn:HWND);
begin
  BtnSetChecked(DeleteCB, False);
  BtnSetChecked(BackupCB, False);
  BtnSetChecked(NoneCB, True);
end;

function ReleaseCapture(): Longint; external 'ReleaseCapture@user32.dll stdcall';
procedure WizardFormOnMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  ReleaseCapture;
  SendMessage(WizardForm.Handle,$0112,$F012,0);
end;

type
  TComponentDesc = record
  Description: String;
  ImageName: String;
  Index: Integer;
end;

var
  CompDescs: array of TComponentDesc;
  CompIndex, LastIndex: Integer;

procedure ShowCompDescription(Sender: TObject; X, Y, Index: Integer; Area: TItemArea);
var
  i: Integer;
begin
  if Index = LastIndex then Exit;
  CompIndex:=-1;
  for i:=0 to GetArrayLength(CompDescs) -1 do
  begin
    if (CompDescs[i].Index = Index) then
    begin
      CompIndex:=i;
      Break;
    end;
  end;
  if (CompIndex >= 0) and (Area = iaItem) then
  begin
    if not FileExists(ExpandConstant('{tmp}\') + CompDescs[CompIndex].ImageName) then
      ExtractTemporaryFile(CompDescs[CompIndex].ImageName);
    ModsImg:=ImgLoad(WizardForm.Handle, ExpandConstant('{tmp}\') + CompDescs[CompIndex].ImageName, 26, 57, 298, 278, True, True);
    ImgApplyChanges(WizardForm.Handle);
    AboutMods.Caption:=CompDescs[CompIndex].Description;
  end else
  begin
    AboutMods.Caption:=CustomMessage('ComponentsInfo');
    ModsImg:=ImgLoad(WizardForm.Handle, ExpandConstant('{tmp}\mods.png') + CompDescs[CompIndex].ImageName, 26, 57, 298, 278, True, True);
    ImgApplyChanges(WizardForm.Handle);
  end;
  LastIndex:=Index;
end;
procedure CompListMouseLeave(Sender: TObject);
begin
  ModsImg:=ImgLoad(WizardForm.Handle, ExpandConstant('{tmp}\mods.png'), 26, 57, 298, 278, True, True);
  ImgApplyChanges(WizardForm.Handle);
  AboutMods.Caption:=CustomMessage('ComponentsInfo');
  LastIndex:=-1;
end;
procedure AddCompDescription(AIndex: Integer; ADescription: String; AImageName: String);
var
  i: Integer;
begin
  i:=GetArrayLength(CompDescs);
  SetArrayLength(CompDescs, i + 1);
  CompDescs[i].Description:=ADescription;
  CompDescs[i].ImageName:=AImageName;
  CompDescs[i].Index:=AIndex - 1;
end;

procedure CompDescription;
begin
  AddCompDescription(1, CustomMessage('CompDesc1'), 'CompDescImg1.png');
  AddCompDescription(2, CustomMessage('CompDesc2'), 'CompDescImg2.png');
  AddCompDescription(3, CustomMessage('CompDesc3'), 'CompDescImg3.png');
  AddCompDescription(4, CustomMessage('CompDesc4'), 'CompDescImg4.png');
  AddCompDescription(5, CustomMessage('CompDesc5'), 'CompDescImg5.png');
  AddCompDescription(6, CustomMessage('CompDesc6'), 'CompDescImg6.png');
  AddCompDescription(7, CustomMessage('CompDesc7'), 'CompDescImg7.png');
  AddCompDescription(8, CustomMessage('CompDesc8'), 'CompDescImg8.png');
  AddCompDescription(9, CustomMessage('CompDesc9'), 'CompDescImg9.png');
  AddCompDescription(10, CustomMessage('CompDesc10'), 'CompDescImg10.png');
  AddCompDescription(11, CustomMessage('CompDesc11'), 'CompDescImg11.png');
  AddCompDescription(12, CustomMessage('CompDesc12'), 'CompDescImg12.png');
  AddCompDescription(13, CustomMessage('CompDesc13'), 'CompDescImg13.png');
  AddCompDescription(14, CustomMessage('CompDesc14'), 'CompDescImg14.png');
  AddCompDescription(15, CustomMessage('CompDesc15'), 'CompDescImg15.png');
  AddCompDescription(16, CustomMessage('CompDesc16'), 'CompDescImg16.png');
  AddCompDescription(17, CustomMessage('CompDesc17'), 'CompDescImg17.png');
  AddCompDescription(18, CustomMessage('CompDesc18'), 'CompDescImg18.png');
  AddCompDescription(19, CustomMessage('CompDesc19'), 'CompDescImg19.png');
  AddCompDescription(20, CustomMessage('CompDesc20'), 'CompDescImg20.png');
  AddCompDescription(21, CustomMessage('CompDesc21'), 'CompDescImg21.png');
  AddCompDescription(22, CustomMessage('CompDesc22'), 'CompDescImg22.png');
  AddCompDescription(23, CustomMessage('CompDesc23'), 'CompDescImg23.png');
  AddCompDescription(24, CustomMessage('CompDesc24'), 'CompDescImg24.png');
  AddCompDescription(25, CustomMessage('CompDesc25'), 'CompDescImg25.png');
  AddCompDescription(26, CustomMessage('CompDesc26'), 'CompDescImg26.png');
  AddCompDescription(27, CustomMessage('CompDesc27'), 'CompDescImg27.png');
  AddCompDescription(28, CustomMessage('CompDesc28'), 'CompDescImg28.png');
  AddCompDescription(29, CustomMessage('CompDesc29'), 'CompDescImg29.png');
  AddCompDescription(30, CustomMessage('CompDesc30'), 'CompDescImg30.png');
  AddCompDescription(31, CustomMessage('CompDesc31'), 'CompDescImg31.png');
  AddCompDescription(32, CustomMessage('CompDesc32'), 'CompDescImg32.png');
  AddCompDescription(33, CustomMessage('CompDesc33'), 'CompDescImg33.png');
  AddCompDescription(34, CustomMessage('CompDesc34'), 'CompDescImg34.png');
  AddCompDescription(35, CustomMessage('CompDesc35'), 'CompDescImg35.png');
  AddCompDescription(36, CustomMessage('CompDesc36'), 'CompDescImg36.png');
  AddCompDescription(37, CustomMessage('CompDesc37'), 'CompDescImg37.png');
  AddCompDescription(38, CustomMessage('CompDesc38'), 'CompDescImg38.png');
  AddCompDescription(39, CustomMessage('CompDesc39'), 'CompDescImg39.png');
  AddCompDescription(40, CustomMessage('CompDesc40'), 'CompDescImg40.png');
  AddCompDescription(41, CustomMessage('CompDesc41'), 'CompDescImg41.png');
  AddCompDescription(42, CustomMessage('CompDesc42'), 'CompDescImg42.png');
  AddCompDescription(43, CustomMessage('CompDesc43'), 'CompDescImg43.png');
  AddCompDescription(44, CustomMessage('CompDesc44'), 'CompDescImg44.png');
  AddCompDescription(45, CustomMessage('CompDesc45'), 'CompDescImg45.png');
  AddCompDescription(46, CustomMessage('CompDesc46'), 'CompDescImg46.png');
  AddCompDescription(47, CustomMessage('CompDesc47'), 'CompDescImg47.png');
  AddCompDescription(48, CustomMessage('CompDesc48'), 'CompDescImg48.png');
  AddCompDescription(49, CustomMessage('CompDesc49'), 'CompDescImg49.png');
  AddCompDescription(50, CustomMessage('CompDesc50'), 'CompDescImg50.png');
  AddCompDescription(51, CustomMessage('CompDesc51'), 'CompDescImg51.png');
  AddCompDescription(52, CustomMessage('CompDesc52'), 'CompDescImg52.png');
  AddCompDescription(53, CustomMessage('CompDesc53'), 'CompDescImg53.png');
  AddCompDescription(54, CustomMessage('CompDesc54'), 'CompDescImg54.png');
  AddCompDescription(55, CustomMessage('CompDesc55'), 'CompDescImg55.png');
  AddCompDescription(56, CustomMessage('CompDesc56'), 'CompDescImg56.png');
  AddCompDescription(57, CustomMessage('CompDesc57'), 'CompDescImg57.png');
  AddCompDescription(58, CustomMessage('CompDesc58'), 'CompDescImg58.png');
  AddCompDescription(59, CustomMessage('CompDesc59'), 'CompDescImg59.png');
  AddCompDescription(60, CustomMessage('CompDesc60'), 'CompDescImg60.png');
  AddCompDescription(61, CustomMessage('CompDesc61'), 'CompDescImg61.png');
  AddCompDescription(62, CustomMessage('CompDesc62'), 'CompDescImg62.png');
  AddCompDescription(63, CustomMessage('CompDesc63'), 'CompDescImg63.png');
  AddCompDescription(64, CustomMessage('CompDesc64'), 'CompDescImg64.png');
  AddCompDescription(65, CustomMessage('CompDesc65'), 'CompDescImg65.png');
  AddCompDescription(66, CustomMessage('CompDesc66'), 'CompDescImg66.png');
  AddCompDescription(67, CustomMessage('CompDesc67'), 'CompDescImg67.png');
  AddCompDescription(68, CustomMessage('CompDesc68'), 'CompDescImg68.png');
  AddCompDescription(69, CustomMessage('CompDesc69'), 'CompDescImg69.png');
  AddCompDescription(70, CustomMessage('CompDesc70'), 'CompDescImg70.png');
  AddCompDescription(71, CustomMessage('CompDesc71'), 'CompDescImg71.png');
  AddCompDescription(72, CustomMessage('CompDesc72'), 'CompDescImg72.png');
  AddCompDescription(73, CustomMessage('CompDesc73'), 'CompDescImg73.png');
  AddCompDescription(74, CustomMessage('CompDesc74'), 'CompDescImg74.png');
  AddCompDescription(75, CustomMessage('CompDesc75'), 'CompDescImg75.png');
  AddCompDescription(76, CustomMessage('CompDesc76'), 'CompDescImg76.png');
  AddCompDescription(77, CustomMessage('CompDesc77'), 'CompDescImg77.png');
  AddCompDescription(78, CustomMessage('CompDesc78'), 'CompDescImg78.png');
  AddCompDescription(79, CustomMessage('CompDesc79'), 'CompDescImg79.png');
  AddCompDescription(80, CustomMessage('CompDesc80'), 'CompDescImg80.png');
  AddCompDescription(81, CustomMessage('CompDesc81'), 'CompDescImg81.png');
  AddCompDescription(82, CustomMessage('CompDesc82'), 'CompDescImg82.png');
  AddCompDescription(83, CustomMessage('CompDesc83'), 'CompDescImg83.png');
  AddCompDescription(84, CustomMessage('CompDesc84'), 'CompDescImg84.png');
  AddCompDescription(85, CustomMessage('CompDesc85'), 'CompDescImg85.png');
  AddCompDescription(86, CustomMessage('CompDesc86'), 'CompDescImg86.png');
  AddCompDescription(87, CustomMessage('CompDesc87'), 'CompDescImg87.png');
  AddCompDescription(88, CustomMessage('CompDesc88'), 'CompDescImg88.png');
  AddCompDescription(89, CustomMessage('CompDesc89'), 'CompDescImg89.png');
  AddCompDescription(90, CustomMessage('CompDesc90'), 'CompDescImg90.png');
  AddCompDescription(91, CustomMessage('CompDesc91'), 'CompDescImg91.png');
  AddCompDescription(92, CustomMessage('CompDesc92'), 'CompDescImg92.png');
  AddCompDescription(93, CustomMessage('CompDesc93'), 'CompDescImg93.png');
  AddCompDescription(94, CustomMessage('CompDesc94'), 'CompDescImg94.png');
  AddCompDescription(95, CustomMessage('CompDesc95'), 'CompDescImg95.png');
  AddCompDescription(96, CustomMessage('CompDesc96'), 'CompDescImg96.png');
  AddCompDescription(97, CustomMessage('CompDesc97'), 'CompDescImg97.png');
  AddCompDescription(98, CustomMessage('CompDesc98'), 'CompDescImg98.png');
  AddCompDescription(99, CustomMessage('CompDesc99'), 'CompDescImg99.png');
end;

procedure InitializeWizard;
begin
  CompDescription;

  ExtractTemporaryFile('mnz.png');
  ExtractTemporaryFile('edit.png');
  ExtractTemporaryFile('mods.png');
  ExtractTemporaryFile('close.png');
  ExtractTemporaryFile('modsbg.png');
  ExtractTemporaryFile('folder.png');
  ExtractTemporaryFile('button.png');
  ExtractTemporaryFile('backup.png');
  ExtractTemporaryFile('checkbox.png');
  ExtractTemporaryFile('backupbg.png');
  ExtractTemporaryFile('CompListBg.png');
  ExtractTemporaryFile('background.png');
  ExtractTemporaryFile('aboutmodsbg.png');

  if not FontExists('{#FontName}') then begin
    ExtractTemporaryFile('{#Font}');
    AddFontResource(ExpandConstant('{tmp}\{#Font}'), FR_PRIVATE, 0);
  end;

  Font:=TFont.Create;
  with Font do begin
    Name:='Open Sans';
    Size:=9;
  end;

  with WizardForm do begin
    Width:=640;
    Height:=480;
    Position:=poScreenCenter;
    BorderStyle:=bsNone;
    CancelButton.SetBounds(0, 0, 0, 0);
    NextButton.SetBounds(0, 0, 0, 0);
    BackButton.SetBounds(0, 0, 0, 0);
    InnerNotebook.Hide;
    OuterNotebook.Hide;
    Bevel.Hide;
    OnMouseDown:=@WizardFormOnMouseDown;
  end;

  TitleLabel:=TLabel.Create(WizardForm);
  with TitleLabel do begin
    AutoSize:=False;
    SetBounds(15, 6, 162, 22);
    Transparent:=True;
    Font.Color:=$E5E5E5;
    Font.Name:='Open Sans';
    Font.Size:=10;
    Caption:='DeSeRtod ModPack {#Patch}';
    Parent:=WizardForm;
  end;
  VersionLabel:=TLabel.Create(WizardForm);
  with VersionLabel do begin
    AutoSize:=False;
    SetBounds(15, 23, 60, 20);
    Transparent:=True;
    Font.Color:=$808080;
    Font.Name:='Open Sans';
    Font.Size:=8;
    Caption:='������ {#Version}';
    Parent:=WizardForm;
  end;

  SelectDirLabel1:=TLabel.Create(WizardForm);
  with SelectDirLabel1 do begin
    AutoSize:=False;
    SetBounds(25, 52, 590, 22);
    Transparent:=True;
    Font.Color:=$E5E5E5;
    Font.Name:='Open Sans';
    Font.Size:=9;
    Caption:='��������� ��������� DeSeRtod ModPack {#Patch} � ��������� �����.';
    Parent:=WizardForm;
  end;
  SelectDirLabel2:=TLabel.Create(WizardForm);
  with SelectDirLabel2 do begin
    AutoSize:=False;
    SetBounds(25, 71, 590, 22);
    Transparent:=True;
    Font.Color:=$E5E5E5;
    Font.Name:='Open Sans';
    Font.Size:=9;
    Caption:='������� ������, ����� ����������. ���� ������ ������� ������ ����� ������� ������.';
    Parent:=WizardForm;
  end;
  SelectDirLabel3:=TLabel.Create(WizardForm);
  with SelectDirLabel3 do begin
    AutoSize:=False;
    SetBounds(25, 150, 590, 50);
    Transparent:=True;
    Font.Color:=$E5E5E5;
    Font.Name:='Open Sans';
    Font.Size:=9;
    Caption:='�� ��������� ������� ������������� � ���������� ����� �������������, �������������'+#13+'������� ��� ������������� � ������� �������.';
    Parent:=WizardForm;
  end;
  SelectDirLabel4:=TLabel.Create(WizardForm);
  with SelectDirLabel4 do begin
    AutoSize:=False;
    SetBounds(25, 193, 590, 50);
    Transparent:=True;
    Font.Color:=$E5E5E5;
    Font.Name:='Open Sans';
    Font.Size:=9;
    Caption:='�������� ����������� ����� �� ��������������. ��� ���� ����� ������� ���������� �����'+#13+'"/res_mods" ��� ������� ��������� ����� ����� �����������.';
    Parent:=WizardForm;
  end;

  DeleteLabel:=TLabel.Create(WizardForm);
  with DeleteLabel do begin
    AutoSize:=False;
    SetBounds(70, 260, 90, 22);
    Transparent:=True;
    Font.Color:=$E5E5E5;
    Font.Name:='Open Sans';
    Font.Size:=9;
    Caption:='������� ����';
    Parent:=WizardForm;
  end;
  BackupLabel:=TLabel.Create(WizardForm);
  with BackupLabel do begin
    AutoSize:=False;
    SetBounds(70, 308, 210, 22);
    Transparent:=True;
    Font.Color:=$E5E5E5;
    Font.Name:='Open Sans';
    Font.Size:=9;
    Caption:='������� ��������� ����� ������';
    Parent:=WizardForm;
  end;
  NoneLabel:=TLabel.Create(WizardForm);
  with NoneLabel do begin
    AutoSize:=False;
    SetBounds(70, 356, 110, 22);
    Transparent:=True;
    Font.Color:=$E5E5E5;
    Font.Name:='Open Sans';
    Font.Size:=9;
    Caption:='������ �� �������';
    Parent:=WizardForm;
  end;

  AboutDeleteLabel:=TLabel.Create(WizardForm);
  with AboutDeleteLabel do begin
    AutoSize:=False;
    SetBounds(70, 279, 240, 22);
    Transparent:=True;
    Font.Color:=$808080;
    Font.Name:='Open Sans';
    Font.Size:=9;
    Caption:='���������� ������ ��� ����� � ������.';
    Parent:=WizardForm;
  end;
  AboutBackupLabel:=TLabel.Create(WizardForm);
  with AboutBackupLabel do begin
    AutoSize:=False;
    SetBounds(70, 327, 365, 22);
    Transparent:=True;
    Font.Color:=$808080;
    Font.Name:='Open Sans';
    Font.Size:=9;
    Caption:='���������� ������� ��������� ����� ���� ������ � ������.';
    Parent:=WizardForm;
  end;
  AboutNoneLabel:=TLabel.Create(WizardForm);
  with AboutNoneLabel do begin
    AutoSize:=False;
    SetBounds(70, 375, 200, 22);
    Transparent:=True;
    Font.Color:=$808080;
    Font.Name:='Open Sans';
    Font.Size:=9;
    Caption:='���������� ������� �� ��� ����.';
    Parent:=WizardForm;
  end;

  AboutMods:=TLabel.Create(WizardForm);
  with AboutMods do begin
    AutoSize:=False;
    SetBounds(35, 352, 280, 44);
    Transparent:=True;
    Font.Color:=$E5E5E5;
    Font.Name:='Open Sans';
    Font.Size:=8;
    Caption:=CustomMessage('ComponentsInfo');
    Parent:=WizardForm;
    WordWrap:=True;
  end;

  with WizardForm.DirEdit do begin
    AutoSize:=False;
    SetBounds(94, 112, 408, 15);
    Parent:=WizardForm;
    BorderStyle:= bsNone;
    Font.Name:='Open Sans';
    Color:=$2C2929;
    Font.Color:=$E5E5E5;
  end;

  with WizardForm.ComponentsList do begin
    SetBounds(343, 57, 271, 352);
    Parent:=WizardForm;
    Color:=$212020;
    BorderStyle:= bsNone;
    Font.Name:='Open Sans';
    Font.Size:=8;
    Font.Color:=$E5E5E5;
    Showroot:=False;
    TreeViewStyle:=False;
    OnItemMouseMove:=@ShowCompDescription;
    OnMouseLeave:=@CompListMouseLeave;
  end;

  CancelBtn:=BtnCreate(WizardForm.Handle, 546, 444, 80, 23, ExpandConstant('{tmp}\button.png'), 0, False);
  BtnSetEvent(CancelBtn,BtnClickEventID,WrapBtnCallback(@CancelButtonOnClick,1));
  BtnSetText(CancelBtn,'�����');
  BtnSetFont(CancelBtn,Font.Handle);
  BtnSetFontColor(CancelBtn,$E5E5E5,$E5E5E5,$E5E5E5,$CCCCCC);

  NextBtn:=BtnCreate(WizardForm.Handle, 452, 444, 80, 23, ExpandConstant('{tmp}\button.png'), 0, False);
  BtnSetEvent(NextBtn,BtnClickEventID,WrapBtnCallback(@NextButtonOnClick,1));
  BtnSetFont(NextBtn,Font.Handle);
  BtnSetFontColor(NextBtn,$E5E5E5,$E5E5E5,$E5E5E5,$CCCCCC);

  BackBtn:=BtnCreate(WizardForm.Handle, 363, 444, 80, 23, ExpandConstant('{tmp}\button.png'), 0, False);
  BtnSetEvent(BackBtn,BtnClickEventID,WrapBtnCallback(@BackButtonOnClick,1));
  BtnSetText(BackBtn,'�����');
  BtnSetFont(BackBtn,Font.Handle);
  BtnSetFontColor(BackBtn,$E5E5E5,$E5E5E5,$E5E5E5,$CCCCCC);

  FAQBtn:=BtnCreate(WizardForm.Handle, 15, 444, 80, 23, ExpandConstant('{tmp}\button.png'), 0, False);
  BtnSetEvent(FAQBtn,BtnClickEventID,WrapBtnCallback(@FAQOnClick,1));
  BtnSetText(FAQBtn, 'F.A.Q.');
  BtnSetFont(FAQBtn,Font.Handle);
  BtnSetFontColor(FAQBtn, $E5E5E5,$E5E5E5,$E5E5E5,$CCCCCC);

  BrowseBtn:=BtnCreate(WizardForm.Handle, 525, 108, 80, 23, ExpandConstant('{tmp}\button.png'), 0, False);
  BtnSetEvent(BrowseBtn,BtnClickEventID,WrapBtnCallback(@BrowseButtonOnClick,1));
  BtnSetText(BrowseBtn, '�����...');
  BtnSetFont(BrowseBtn,Font.Handle);
  BtnSetFontColor(BrowseBtn, $E5E5E5,$E5E5E5,$E5E5E5,$CCCCCC);

  CloseBtn:=BtnCreate(WizardForm.Handle, 613, 7, 20, 20, ExpandConstant('{tmp}\close.png'), 0, False);
  BtnSetEvent(CloseBtn,BtnClickEventID,WrapBtnCallback(@CancelButtonOnClick,1));

  MinBtn:=BtnCreate(WizardForm.Handle, 592, 7, 20, 20, ExpandConstant('{tmp}\mnz.png'), 0, False);
  BtnSetEvent(MinBtn,BtnClickEventID,WrapBtnCallback(@MinButtonOnClick,1));

  DeleteCB:=BtnCreate(WizardForm.Handle, 50, 262, 230, 13, ExpandConstant('{tmp}\checkbox.png'), 0, True);
  BtnSetEvent(DeleteCB,BtnClickEventID,WrapBtnCallback(@DeleteOnClick,1));
  BackupCB:=BtnCreate(WizardForm.Handle, 50, 310, 230, 13, ExpandConstant('{tmp}\checkbox.png'), 0, True);
  BtnSetEvent(BackupCB,BtnClickEventID,WrapBtnCallback(@BackupOnClick,1));
  BtnSetChecked(BackupCB,True);
  NoneCB:=BtnCreate(WizardForm.Handle, 50, 358, 230, 13, ExpandConstant('{tmp}\checkbox.png'), 0, True);
  BtnSetEvent(NoneCB,BtnClickEventID,WrapBtnCallback(@NoneOnClick,1));

  BackgroundImg:=ImgLoad(WizardForm.Handle, ExpandConstant('{tmp}\background.png'), 0, 0, 640, 480, True, True);
  DirEditImg:=ImgLoad(WizardForm.Handle, ExpandConstant('{tmp}\edit.png'), 86, 108, 424, 23, True, True);
  FolderImg:=ImgLoad(WizardForm.Handle, ExpandConstant('{tmp}\folder.png'), 35, 104, 36, 30, True, True);
  BackupBgImg:=ImgLoad(WizardForm.Handle, ExpandConstant('{tmp}\backupBg.png'), 25, 243, 590, 167, True, True);
  DeleteImg:=ImgLoad(WizardForm.Handle, ExpandConstant('{tmp}\backup.png'), 558, 265, 32, 26, True, True);
  BackupImg:=ImgLoad(WizardForm.Handle, ExpandConstant('{tmp}\backup.png'), 558, 313, 32, 26, True, True);
  NoneImg:=ImgLoad(WizardForm.Handle, ExpandConstant('{tmp}\backup.png'), 558, 361, 32, 26, True, True);
  CompListBgImg:=ImgLoad(WizardForm.Handle, ExpandConstant('{tmp}\CompListBg.png'), 335, 56, 280, 354, True, True);
  ModsBgImg:=ImgLoad(WizardForm.Handle, ExpandConstant('{tmp}\modsbg.png'), 25, 56, 300, 280, True, True);
  AboutModsBgImg:=ImgLoad(WizardForm.Handle, ExpandConstant('{tmp}\aboutmodsbg.png'), 25, 346, 300, 64, True, True);
  ModsImg:=ImgLoad(WizardForm.Handle, ExpandConstant('{tmp}\mods.png'), 26, 57, 298, 278, True, True);
end;

procedure CurPageChanged(CurPageID: Integer);
begin
  if CurPageID = wpWelcome then begin
    BtnSetText(NextBtn, '�����');
    BtnSetVisibility(BackBtn, False);
  end else begin
    BtnSetVisibility(BackBtn, True);
  end;

  if CurPageID = wpSelectDir then begin
    BtnSetText(NextBtn, '�����');
    BtnSetVisibility(BrowseBtn, True);
    BtnSetVisibility(DeleteCB, True);
    BtnSetVisibility(BackupCB, True);
    BtnSetVisibility(NoneCB, True);
    ImgSetVisibility(DirEditImg, True);
    ImgSetVisibility(DeleteImg, True);
    ImgSetVisibility(BackupImg, True);
    ImgSetVisibility(NoneImg, True);
    ImgSetVisibility(FolderImg, True);
    ImgSetVisibility(BackupBgImg, True);
    SelectDirLabel1.Visible:=True;
    SelectDirLabel2.Visible:=True;
    SelectDirLabel3.Visible:=True;
    SelectDirLabel4.Visible:=True;
    DeleteLabel.Visible:=True;
    BackupLabel.Visible:=True;
    NoneLabel.Visible:=True;
    AboutDeleteLabel.Visible:=True;
    AboutBackupLabel.Visible:=True;
    AboutNoneLabel.Visible:=True;
    WizardForm.DirEdit.Visible:=True;
  end else begin
    BtnSetVisibility(BrowseBtn, False);
    BtnSetVisibility(DeleteCB, False);
    BtnSetVisibility(BackupCB, False);
    BtnSetVisibility(NoneCB, False);
    ImgSetVisibility(FolderImg, False);
    ImgSetVisibility(DirEditImg, False);
    ImgSetVisibility(BackupBgImg, False);
    ImgSetVisibility(DeleteImg, False);
    ImgSetVisibility(BackupImg, False);
    ImgSetVisibility(NoneImg, False);
    SelectDirLabel1.Visible:=False;
    SelectDirLabel2.Visible:=False;
    SelectDirLabel3.Visible:=False;
    SelectDirLabel4.Visible:=False;
    DeleteLabel.Visible:=False;
    BackupLabel.Visible:=False;
    NoneLabel.Visible:=False;
    AboutDeleteLabel.Visible:=False;
    AboutBackupLabel.Visible:=False;
    AboutNoneLabel.Visible:=False;
    WizardForm.DirEdit.Visible:=False;
  end;

  if CurPageID = wpSelectComponents then begin
    BtnSetText(NextBtn, '���������');
    ImgSetVisibility(CompListBgImg, True);
    ImgSetVisibility(ModsBgImg, True);
    ImgSetVisibility(ModsImg, True);
    ImgSetVisibility(AboutModsBgImg, True);
    WizardForm.ComponentsList.Visible:=True;
    AboutMods.Visible:=True;
  end else begin
    ImgSetVisibility(CompListBgImg, False);
    ImgSetVisibility(ModsBgImg, False);
    ImgSetVisibility(ModsImg, False);
    ImgSetVisibility(AboutModsBgImg, False);
    WizardForm.ComponentsList.Visible:=False;
    AboutMods.Visible:=False;
  end;

  if CurPageID = wpInstalling then begin
    BtnSetVisibility(NextBtn, False);
    BtnSetVisibility(BackBtn, False);
    DeleteCheck;
    BackupCheck;
  end else begin
    BtnSetVisibility(NextBtn, True);
  end;

  if CurPageID = wpFinished then begin
    BtnSetText(NextBtn, 'Finish Him!');
    BtnSetVisibility(BackBtn, False);
    BtnSetVisibility(CancelBtn, False);
  end else begin
  end;
  ImgApplyChanges(WizardForm.Handle);
end;

procedure DeinitializeSetup;
begin
  RemoveFontResource(ExpandConstant('{tmp}\{#Font}'), FR_PRIVATE, 0);
  Font.Free;
  gdipShutDown;
end;